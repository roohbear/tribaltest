//
//  GregViewController.m
//  TribalScaleApp
//
//  Created by Greg on 2016-03-28.
//  Copyright © 2016 Rooh Corp. All rights reserved.
//

#import "GregViewController.h"


////////////////////////////////////////////
@implementation UserCell

- (void)setLabels:(UserInfo *)userInfo
{
	self.imageView.image = userInfo.image;
	self.labelName.text = [NSString stringWithFormat:@"%@ %@", userInfo.nameFirst, userInfo.nameLast];
	self.labelAddress.text = [NSString stringWithFormat:@"%@, %@", userInfo.addressCity, userInfo.addressState];
	self.labelEmail.text = userInfo.email;
}

@end

////////////////////////////////////////////
@implementation UserInfo

@end

////////////////////////////////////////////
@interface GregViewController ()

@property (nonatomic, retain) IBOutlet UITableView	*tableUsers;			// an array of userInfo objects
@property (nonatomic, retain) IBOutlet UIImageView	*imageviewBackground;	// an array of userInfo objects
@property (nonatomic, retain) IBOutlet UIButton		*buttonAddUser;
@property (nonatomic, retain) NSMutableArray		*arrUsers;

@property (nonatomic, assign) int					nBackgroundImageNum;
@property (nonatomic, assign) int					nBackgroundImageNumDirection;

@end


/////////////////////////////////////////////
@implementation GregViewController

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.navigationController.navigationBar.hidden = YES;
	
	self.arrUsers = [[NSMutableArray alloc] init];
	
	self.nBackgroundImageNum = 1;
	self.nBackgroundImageNumDirection = 1;
	[self performSelector:@selector(changeBackground) withObject:nil afterDelay:0.1];
	
	self.buttonAddUser.layer.cornerRadius = 10.0;
	self.buttonAddUser.layer.borderColor = [[UIColor darkGrayColor] CGColor];
	self.buttonAddUser.backgroundColor = [UIColor whiteColor];

	[self getRandomUser];
}

// called on main thread every few seconds to cycle the background graphic
- (void)changeBackground
{
	self.nBackgroundImageNum += self.nBackgroundImageNumDirection;
	if(self.nBackgroundImageNum > 72 || self.nBackgroundImageNum == 1) {
		self.nBackgroundImageNumDirection *= -1;
	}
	
	NSString *strFilename = [NSString stringWithFormat:@"backgrounds/ace_%03d.jpg", self.nBackgroundImageNum];
	self.imageviewBackground.image = [UIImage imageNamed:strFilename];
	
	// come back in a second and do it again...
	[self performSelector:@selector(changeBackground) withObject:nil afterDelay:0.1];
}

- (IBAction)buttonAddUserClicked:(id)sender
{
	[self getRandomUser];
}

// called on main thread to download a random user from https://randomuser.me/api/
- (void)getRandomUser
{
	[self performSelectorInBackground:@selector(getRandomUserBackground) withObject:nil];
}

// called on background thread to download a random user
- (void)getRandomUserBackground
{
	NSError *err = nil;
	NSURL *url = [[NSURL alloc] initWithString:@"https://randomuser.me/api/"];
	NSData	*dataRaw = [[NSData alloc] initWithContentsOfURL:url];
	if(dataRaw.length > 0) {
		// dive into the the result to get the relevant user info I want
		NSDictionary *dictRoot = [NSJSONSerialization JSONObjectWithData:dataRaw options:NSJSONReadingMutableLeaves error:&err];
		NSArray *arrResults = dictRoot[@"results"];
		NSDictionary *resultZero = arrResults[0];
		NSDictionary *resultZeroUser = resultZero[@"user"];
		NSDictionary *dictName = resultZeroUser[@"name"];
		NSDictionary *dictPicture = resultZeroUser[@"picture"];
		NSDictionary *dictLocation = resultZeroUser[@"location"];
		
		// download the image
		NSURL *urlImage = [[NSURL alloc] initWithString:dictPicture[@"medium"]];
		NSData *dataImage = [[NSData alloc] initWithContentsOfURL:urlImage];
		UIImage *image = [UIImage imageWithData:dataImage];
		
		// make a UserInfo object, fill it in, and add it to self.arrUsers
		UserInfo *userInfo = [[UserInfo alloc] init];
		userInfo.nameTitle = dictName[@"title"];
		userInfo.nameFirst = dictName[@"first"];
		userInfo.nameLast = dictName[@"last"];
		userInfo.image = image;
		userInfo.email = resultZeroUser[@"email"];
		userInfo.addressCity = dictLocation[@"city"];
		userInfo.addressState = dictLocation[@"state"];
		[self.arrUsers addObject:userInfo];
	}
	
	[self performSelectorOnMainThread:@selector(getRandomUserBackgroundDone) withObject:nil waitUntilDone:NO];
}

// called on main thread when getRandomUserBackground is done
- (void)getRandomUserBackgroundDone
{
	// add the user to the array of users and refresh the table
	[self.tableUsers reloadData];
}

#pragma mark - UITableView datasource and delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.arrUsers.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 110.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UserCell *ret = [tableView dequeueReusableCellWithIdentifier:@"gregCell"];
	if(!ret) {
		ret = [[UserCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"gregCell"];
	}
	
	UserInfo *userInfo = self.arrUsers[indexPath.row];
	[ret setLabels:userInfo];
	
	return ret;
}

@end
