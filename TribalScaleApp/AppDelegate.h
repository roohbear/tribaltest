//
//  AppDelegate.h
//  TribalScaleApp
//
//  Created by Greg on 2016-03-28.
//  Copyright © 2016 Rooh Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

