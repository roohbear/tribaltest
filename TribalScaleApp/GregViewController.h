//
//  GregViewController.h
//  TribalScaleApp
//
//  Created by Greg on 2016-03-28.
//  Copyright © 2016 Rooh Corp. All rights reserved.
//

#import <UIKit/UIKit.h>


//////////////////////////////////////////////////////////////////////////////
@interface UserCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel		*labelName;
@property (nonatomic, retain) IBOutlet UILabel		*labelAddress;
@property (nonatomic, retain) IBOutlet UILabel		*labelEmail;
@property (nonatomic, retain) IBOutlet UIImageView	*image;

@end


//////////////////////////////////////////////////////////////////////////////
@interface UserInfo : NSObject

@property (nonatomic, retain) NSString *nameTitle;
@property (nonatomic, retain) NSString *nameFirst;
@property (nonatomic, retain) NSString *nameLast;
@property (nonatomic, retain) NSString *addressStreet;
@property (nonatomic, retain) NSString *addressCity;
@property (nonatomic, retain) NSString *addressState;
@property (nonatomic, retain) NSString *addressZip;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) UIImage *image;

@end



/////////////////////////////////////////////////////////////////////////////////////////////
@interface GregViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@end

